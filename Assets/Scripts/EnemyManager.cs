﻿using System;
using UnityEngine;
using  UnityEngine.Tilemaps;
using UnityEditor.Animations;

public class EnemyManager : MonoBehaviour
{
    private bool goesRight; // direction the enemy is going
    private GameObject player;
    private MovementManager movementManager;
    private Tilemap tilemap;
    private Animator animator;
    private Rigidbody2D rigidbody2D;
    private LayerMask projectiles_layer;

    private void Start()
    {
        player = GameObject.Find("Player");
        movementManager = GetComponent<MovementManager>();
        goesRight = player.transform.position.x > transform.position.x;
        tilemap = GameObject.Find("Tilemap").GetComponent<Tilemap>();
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        projectiles_layer = LayerMask.GetMask("Projectiles");
    }

    private void Update()
    {
        Vector3 turnAroundDetector = transform.position + Vector3.down + (goesRight ? Vector3.right : Vector3.left);
        var tile = tilemap.GetTile(tilemap.WorldToCell(turnAroundDetector));
        if (tile==null)
        {
            goesRight = !goesRight;
        }

        if (goesRight)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        if (movementManager.GetGrounded())
        {
            if (goesRight)
            {
                movementManager.moveRight();
            }
            else
            {
                movementManager.moveLeft();
            }
        }
        if (rigidbody2D.IsTouchingLayers(projectiles_layer))
        {
            // set hit for animation
            animator.SetTrigger("dying");
            Destroy(gameObject, 1);
        }
    }
}
