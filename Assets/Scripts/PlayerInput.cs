﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerInput : MonoBehaviour
{
    private MovementManager movementManager;


    [SerializeField] private GameObject projectile;
    [SerializeField]
    private ProjectileManager.Orientation shooting_direction;
    private Transform transform;
    private Animator animator;
    private float fireRate = 0.5f;
    private float nextFire = 0.0f;
    private bool looksRight = true;
    
    // Start is called before the first frame update
    private void Start()
    {
        movementManager = GetComponent<MovementManager>();
        transform = GetComponent<Transform>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (movementManager.GetGrounded())
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                movementManager.moveRight();
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                movementManager.moveLeft();
            }
            else
            {
                movementManager.stopHorizontal();
            }
        }

        if (Input.GetAxis("HShooting") > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
            animator.SetInteger("orientation", 0);
            shooting_direction = ProjectileManager.Orientation.Right;
        }
        else if (Input.GetAxis("HShooting") < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            animator.SetInteger("orientation", 0);
            shooting_direction = ProjectileManager.Orientation.Left;
        }
        if (Input.GetAxis("VShooting") > 0)
        {
            animator.SetInteger("orientation", 1);
            shooting_direction = ProjectileManager.Orientation.Up;
        }
        else if (Input.GetAxis("VShooting") < 0)
        {
            animator.SetInteger("orientation", -1);
            shooting_direction = ProjectileManager.Orientation.Down;
        }

        if (Input.GetButtonDown("Jump"))
        {
            movementManager.jump();
        }
        if (Input.GetButton("Fire") && Time.time > nextFire)
        {
            animator.SetTrigger("shooting");
            nextFire = Time.time + fireRate;
            var new_projectile = Instantiate(projectile, transform.position, transform.rotation);
            var projectile_manager = new_projectile.GetComponent<ProjectileManager>();
            projectile_manager.set_orientation(shooting_direction);
        }
    }
}
