﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    public enum Orientation
    {
        Up,
        Down,
        Left,
        Right
    };
    
    private Orientation orientation;
    private float speed = 5f;
    private int lifetime = 3;

    public void set_orientation(Orientation orientation)
    {
        this.orientation = orientation;
        Rigidbody2D rigidbody2D = GetComponent<Rigidbody2D>();
        Transform transform = GetComponent<Transform>();
        switch (this.orientation)
        {
            case Orientation.Up:
                transform.Rotate(0, 0, 180);
                transform.Translate(new Vector3(0, -0.4f, 0));
                rigidbody2D.velocity = new Vector3(0, speed, 0);
                break;
            case Orientation.Down:
                transform.Translate(new Vector3(0, -0.8f, 0));
                rigidbody2D.velocity = new Vector3(0, -speed, 0);
                break;
            case Orientation.Left:
                transform.Translate(new Vector3(-0.6f, -0.2f, 0));
                transform.Rotate(0, 0, -90);
                rigidbody2D.velocity = new Vector3(-speed, 0,0);
                break;
            case Orientation.Right:
                transform.Translate(new Vector3(0.6f, -0.2f, 0));
                transform.Rotate(0, 0, 90);
                rigidbody2D.velocity = new Vector3(speed, 0,0);
                break;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, lifetime);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
