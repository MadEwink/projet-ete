﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    // singleton model
    public static GameManager Instance;
    
    void Start()
    {
        if (!ReferenceEquals(Instance, null))
            Debug.LogError("There are at least two game managers in the game (singleton)");
        Instance = this;
    }

    public void EndGame()
    {
        SceneManager.LoadScene("gameover");
    }
}
