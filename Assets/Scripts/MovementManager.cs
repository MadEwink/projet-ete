﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class MovementManager : MonoBehaviour
{
    [SerializeField] private float speed = 3f;
    [SerializeField] private float jumpForce = 100f;
    private const float maxJumpSpeed = 10f;
    private bool grounded;
    private BoxCollider2D boxCollider2D;
    private LayerMask enemy_layer;

    private Rigidbody2D rb2d;
    
    // Start is called before the first frame update
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        boxCollider2D = GetComponent<BoxCollider2D>();
        enemy_layer = LayerMask.GetMask("Enemies");
    }

    private void OnValidate()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
    }

    public bool GetGrounded()
    {
        ContactPoint2D[] contacts = new ContactPoint2D[16];
        int nb_contacts = rb2d.GetContacts(contacts);
        grounded = false;
        for (int i = 0; i < nb_contacts; i++)
        {
            Collider2D other_collider = contacts[i].rigidbody == rb2d ? contacts[i].otherCollider : contacts[i].collider;
            if ( other_collider && other_collider.gameObject.layer == LayerMask.NameToLayer("Terrain") // if touches terrain
                && Mathf.Abs(contacts[i].normal.y) > Mathf.Abs(contacts[i].normal.x) // if is not a wall
                && contacts[i].point.y < transform.position.y) // if is under this
            {
                grounded = true;
            }
        }

        return grounded;
    }

    private void OnDrawGizmosSelected()
    {
    }

    private void Update()
    {
        var velocity = rb2d.velocity;
        if (velocity.y > maxJumpSpeed) velocity.y = maxJumpSpeed;
        rb2d.velocity = velocity;
        if (transform.position.y <= -10f)
            GameManager.Instance.EndGame();
        if (rb2d.IsTouchingLayers(enemy_layer))
            GameManager.Instance.EndGame();
    }

    public void moveLeft()
    {
        rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
    }
    public void moveRight()
    {
        rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
    }

    public void stopHorizontal()
    {
        rb2d.velocity = new Vector2(0, rb2d.velocity.y);
    }

    public void jump()
    {
        if (GetGrounded())
        {
            rb2d.AddForce(Vector2.up*jumpForce);
        }
    }
}
