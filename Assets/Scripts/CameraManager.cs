﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PathSegment
{
	[SerializeField]
	private float speed = 1f;
	[SerializeField]
	private GameObject trigger; // the object that should trigger the scrolling
	private const float stopPoint = 0.25f;
	[SerializeField][Range(stopPoint,1.0f)]
	private float scrollPoint = 0.25f; // the x coordinate at which the scrolling should begin (at speed speed)
	[SerializeField][Range(stopPoint,1.0f)]
	private float maxSpeedPoint = 0.5f; // the x coordinate at which the scrolling should got at maxspeed since speed is too low
	private float multiplicationModifier; // the x difference with trigger modifies the speed
	private float additionModifier; // smoothness
	private int currentMovement = 0;
	
	public PathSegment()
	{
		this.trigger = null;
	}
	public PathSegment(PathSegment other)
	{
		this.speed = other.speed;
		this.trigger = other.trigger;
		this.scrollPoint = other.scrollPoint;
		this.maxSpeedPoint = other.maxSpeedPoint;
	}
	
	/*
	 * return
	 *  0 if the camera can stay still
	 *  1 if the camera has to scroll
	 *  2 if the camera has to go faster
	 *  if trigger is null, the camera scrolls continuously
	*/
	public int hasToMove(Camera cam)
	{
		multiplicationModifier = 1f; // no modifying
	    if (!trigger)
	    {
		    currentMovement = 1;
		    return currentMovement;
        }
		Vector2 screenCoordinates = cam.WorldToScreenPoint(trigger.transform.position);
		if (screenCoordinates.x > maxSpeedPoint*cam.pixelWidth) {
			currentMovement = 2;
			additionModifier = -speed * (maxSpeedPoint - scrollPoint);
		} else if (screenCoordinates.x > scrollPoint*cam.pixelWidth && currentMovement != 2) {
			currentMovement = 1;
		} else if (screenCoordinates.x < stopPoint*cam.pixelWidth) {
			currentMovement = 0;
		}
		multiplicationModifier = (screenCoordinates.x/cam.pixelWidth) - scrollPoint;
		return currentMovement;
	}
	
	public float getSpeed() {
		return currentMovement*speed*multiplicationModifier - additionModifier;
	}
}

[System.Serializable]
public class PathPoint
{
	[SerializeField]
	private Vector2 coordinates;
	
	public Vector3 getCoordinates3() {
		return new Vector3(coordinates.x, coordinates.y, 0);
	}
	public Vector2 getCoordinates() {
		return coordinates;
	}
}

public class CameraManager : MonoBehaviour
{
	[SerializeField]
	private GameObject self_camera;
	private Camera cam;
	[SerializeField]
	private PathPoint[] points;
	[SerializeField]
	private PathSegment[] pathSegment;
	private int lastPoint = 0;
	
	[SerializeField]
	private int pathSegmentFocus;
	
	private int MakeScroll()
	{
		if (lastPoint >= points.Length-1) return 0;
		if (pathSegment[lastPoint].hasToMove(cam) == 0) return 0;
		Vector2 direction = points[lastPoint+1].getCoordinates() - points[lastPoint].getCoordinates();
		Vector2 maxTranslation = points[lastPoint+1].getCoordinates() - new Vector2(self_camera.transform.position.x, self_camera.transform.position.y);
		direction.Normalize();
		direction *= pathSegment[lastPoint].getSpeed();
		if (direction.magnitude > maxTranslation.magnitude) {
			// we have to set the camera to the next point
			self_camera.transform.Translate(maxTranslation);
			lastPoint ++;
			return 1;
		}
		self_camera.transform.Translate(direction);
		return 0;
	}
	
	private void OnValidate()
	{
		if (points.Length != pathSegment.Length+1)
		{
			var newPathSegment = new PathSegment[points.Length - 1];
			if (pathSegment.Length >= 1)
			{
				for (var i = 0; i < pathSegment.Length && i < newPathSegment.Length; i++)
				{
					newPathSegment[i] = pathSegment[i];
				}
			}
			var j = newPathSegment.Length - pathSegment.Length;
			if (j > 0)
			{
				// there are more points, copy the new points from the last one if it exists
				for (var i = 0; i < j; i++)
				{
					if (pathSegment.Length < 1)
						newPathSegment[i] = new PathSegment();
					else
						newPathSegment[pathSegment.Length + i] = new PathSegment(newPathSegment[pathSegment.Length - 1]);
				}
			}
			pathSegment = newPathSegment;
		}
	}
	
	private void OnDrawGizmosSelected()
    {
		for (var i = 0 ; i < points.Length-1 ; i++) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine(points[i].getCoordinates3(), points[i+1].getCoordinates3());
			Gizmos.color = Color.blue;
			Gizmos.DrawCube(points[i].getCoordinates3(), new Vector3(0.3f, 0.3f, 0));
		}
		Gizmos.DrawCube(points[points.Length-1].getCoordinates3(), new Vector3(0.3f, 0.3f, 0));
	}
    // Start is called before the first frame update
    private void Start()
    {
		self_camera.transform.position = points[0].getCoordinates();
		self_camera.transform.Translate(new Vector3(0f,0f,-10f));
		cam = self_camera.GetComponent<Camera>();
		var scroll_collider = self_camera.GetComponent<BoxCollider2D>();
		scroll_collider.size = new Vector2(1, (cam.ScreenToWorldPoint(new Vector3(0, cam.pixelHeight)) - cam.ScreenToWorldPoint(new Vector3(0,0))).y);
		scroll_collider.offset = new Vector2((cam.ScreenToWorldPoint(new Vector3(0,0))).x-0.4f, 0);
		scroll_collider.offset = new Vector2(-18.3f, 0);
		lastPoint = 0;
    }

    // Update is called once per frame
    private void Update()
    {
        MakeScroll();
    }
}
