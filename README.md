# Sunny weather

Sunny weather is a shooter in which you play a sun which does not want the weather to be rainy. Shoot sun beams at the clouds in order to turn them into rainbows.

## Goal

Our teachers asked us to realise a basic shooter with some features :
- Camera scroll
- Player horizontal movement and jump
- Player four direction shoot
- Ennemies wandering and changing directions when facing an obstacle (gap, wall)

They asked us to do this as our introduction to Unity, following basic tutorials and getting familiar with the tool.

## Work achieved

Basic shooter :
- Player can move and jump
- Player can't change direction while jumping
- Player can shoot in four directions
- Enemies kill the player at touch
- Enemies change direction when arriving to a gap